#1. Registrar datos de personas en un diccionario
#2. Recorrer los Nombres registrados en el diccionario
#3. Eliminar una persona al ingresar la cedula
#4. Consultar el nombre de una persona al registrar la cedula
#5. Cambiar el nombre ingresado una cedula por el usuario
#Debe utilizar procedimientos
#Debe crear un Menú que contenga las opciones solicitadas y
#adicionalmente las opciones de salir e imprimir la documentación de cada
#procedimiento


lista_personas=[]
#Agregar datos a persona para diccionario
def agregar_datos():
    '''Agrega datos para diccionario'''

    print("Registrando datos:\n")

    cedula=int(input("Ingrese cedula: "))
    nombre=input("Ingrese nombre completo: ")
    correo_ele=input("Ingrese correo eléctronico: ")
    ano_nac=int(input("Ingrese año de nacimiento: "))
    tele=int(input("Ingrese número de teléfono(11111111): "))

    diccionario_persona={1:cedula,2:nombre,3:correo_ele,4:ano_nac,5:tele}
    lista_personas.append(diccionario_persona)


#Consulta registrado por medio de cedula en lista
def consultar_datos():
    '''Consulta datos por medio de cedula'''

    print("Consultar datos: ")
    cedula=int(input("Ingrese cedula a consultar: "))
    for persona in lista_personas:
        if cedula==persona.get(1):
            print("Personas consultadas: ")
            print("Cedula: " + str(persona.get(1))+" Nombre: "+persona.get(2)+" Email: " +persona.get(3)+ " Año Nacimiento: "+
                  str(persona.get(4)) +" Telefono: " + str(persona.get(5)))
            return
        else:
            print("La persona no se encuentra registrada")

#modifica datos por medio de cedula en lista
def modificar_datos():
    '''Modifica datos por medio de cedula'''
    print("Modificar datos: ")
    cedula = int(input("Ingrese cedula que desea modificar datos: "))
    contador=0

    while contador< len(lista_personas):
        persona=lista_personas[contador]
        if persona.get(1)==cedula:

            nombre = input("Ingrese nombre completo: ")
            correo_ele = input("Ingrese correo eléctronico: ")
            ano_nac = int(input("Ingrese año de nacimiento: "))
            tele = int(input("Ingrese número de teléfono(11111111): "))

            persona[2]=nombre
            persona[3]=correo_ele
            persona[4]=ano_nac
            persona[5]=tele

            lista_personas[contador]=persona
            return
        else:
            print("La cedula no esta registrada")
        contador+=1
#Elimina registo de lista por medio de cedula
def eliminar_registro():
    '''Elimina registro por medio cedula'''
    print("Eliminar datos: ")
    cedula = int(input("Ingrese cedula para eliminar registro: "))
    for persona in lista_personas:
        if persona.get(1)==cedula:
            lista_personas.remove(persona)
            return
        else:
            print("La cedula no esta registrada")
#Imprime documentacion
def imprimir_doc():
    '''Imprime documentacion'''
    print(menu.__doc__)
    print(agregar_datos.__doc__)
    print(consultar_datos.__doc__)
    print(modificar_datos.__doc__)
    print(eliminar_registro.__doc__)

#menu principal
menu=("1.Registrar datos\n"
      "2.Consultar datos\n"
      "3.Modificar datos\n"
      "4.Eliminar datos\n"
      "5.Imprimir datos\n"
      "6.Salir")
continuar=True
while continuar:
    print(menu)
    opcion=int(input("Digite la opción deseada: "))
    if opcion==1:
        agregar_datos()
    elif opcion==2:
        consultar_datos()
    elif opcion==3:
        modificar_datos()
    elif opcion==4:
        eliminar_registro()
    elif opcion==5:
        imprimir_doc()
    elif opcion==6:
        continuar=False
        print("Gracias por usar el programa")
    else:
        print("La opcion no existe.\n")

